# EPiServerCategoryListPathRestriction
Adds the posibility to mark the EPiServer Category property with a path that defines the root of the category listing

## Installing the library
To install the library just use:  
`PM> Install-Package Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction`  
And install the latest version.  

Just override the Category field and add the attribute with the path corresponding to your category tree like:
```cs
	///  <inheritdoc cref="ICategorizable.Category" />
    [CategoryPathRestriction("Taxonomy/Corporate")]
    public override CategoryList Category { get; set; }
```
![](documentation/categories.png)  
![](documentation/categoryselection.png)  
That's all there is to it.


## [Contributing](Contributing.md)
See the [Contribution guide](Contributing.md) for help about contributing to this project.
  
## [Changelog](Changelog.md)
See the [Changelog](Changelog.md) to see the change history.
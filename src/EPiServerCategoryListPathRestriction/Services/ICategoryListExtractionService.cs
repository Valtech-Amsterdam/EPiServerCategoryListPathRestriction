﻿using System;
using System.Collections.Generic;

namespace Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.Services
{
    public interface ICategoryListExtractionService
    {
        AttributePathResult GetAttributePath(IEnumerable<Attribute> attributeList);
    }
}
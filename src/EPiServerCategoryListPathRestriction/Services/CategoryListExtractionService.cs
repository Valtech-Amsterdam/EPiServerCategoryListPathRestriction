﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EPiServer.DataAbstraction;
using Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.Attributes;

namespace Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.Services
{
    public class CategoryListExtractionService : ICategoryListExtractionService
    {
        private readonly CategoryRepository _categoryRepository;

        /// <inheritdoc />
        public CategoryListExtractionService(CategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public AttributePathResult GetAttributePath(IEnumerable<Attribute> attributeList)
        {
            // Get the categories
            var selectedCategoryRoot = _categoryRepository.GetRoot();
            // Get the first path attribute if one exists
            var pathAttribute = attributeList.FirstOrDefault(attribute => attribute is CategoryPathRestrictionAttribute) as CategoryPathRestrictionAttribute;
            if (pathAttribute == null) return new AttributePathResult
            {
                Valid = true,
                SelectedCategoryRootID = selectedCategoryRoot.ID
            };

            // Split the path into sections for iteration
            var pathArray = pathAttribute.Path.Split(
                new[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar },
                StringSplitOptions.RemoveEmptyEntries);
            // Check if any categories
            if (selectedCategoryRoot?.Categories?.Any() != true) return new AttributePathResult
            {
                Valid = false,
                Path = pathAttribute.Path
            };

            // Loop through the categories and see if the path matches existing categories
            // if the path doesn't end in an existing category bounce and keep the default root
            foreach (var pathChunk in pathArray)
            {
                var categoryFromPath = selectedCategoryRoot.Categories
                    .FirstOrDefault(category => category.Name.Equals(pathChunk));
                if (categoryFromPath == null) return new AttributePathResult
                {
                    Valid = false,
                    Path = pathAttribute.Path
                };

                selectedCategoryRoot = categoryFromPath;
            }
            

            return new AttributePathResult
            {
                Valid = true,
                Path = pathAttribute.Path,
                SelectedCategoryRootID = selectedCategoryRoot.ID
            };
        }
    }

    public class AttributePathResult
    {
        public bool Valid { get; set; }
        public string Path { get; set; }
        public int SelectedCategoryRootID { get; set; }
    }
}

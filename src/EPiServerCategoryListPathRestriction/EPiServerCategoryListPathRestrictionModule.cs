﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.Services;

namespace Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction
{
    /// <inheritdoc cref="IConfigurableModule"/>
    [InitializableModule,
        ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class EPiServerCategoryListPathRestrictionModule : IConfigurableModule
    {
        /// <inheritdoc cref="IConfigurableModule"/>
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Services.AddSingleton<ICategoryListExtractionService, CategoryListExtractionService>();
        }
        /// <inheritdoc cref="IConfigurableModule"/>
        public void Initialize(InitializationEngine context) { }
        /// <inheritdoc cref="IConfigurableModule"/>
        public void Uninitialize(InitializationEngine context) { }
    }
}
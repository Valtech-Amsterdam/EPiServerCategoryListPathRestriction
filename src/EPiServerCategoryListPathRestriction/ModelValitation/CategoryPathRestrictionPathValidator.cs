﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Validation;
using Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.Attributes;
using Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.Services;

namespace Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.ModelValitation
{
    /// <summary>
    /// A custom validator to check whether the 
    /// <see cref="CategoryPathRestrictionAttribute"/>.<see cref="CategoryPathRestrictionAttribute.Path"/> points to an existing catogory
    /// </summary>
    public class CategoryPathRestrictionPathValidator : IValidate<ICategorizable>
    {
        private readonly ICategoryListExtractionService _categoryListExtractionService;

        /// <inheritdoc />
        public CategoryPathRestrictionPathValidator(ICategoryListExtractionService categoryListExtractionService)
        {
            _categoryListExtractionService = categoryListExtractionService;
        }


        /// <inheritdoc />
        public IEnumerable<ValidationError> Validate(ICategorizable vacancyPage)
        {
            var attributeList = vacancyPage.GetType().GetProperty(nameof(ICategorizable.Category))
                ?.GetCustomAttributes(typeof(CategoryPathRestrictionAttribute), false).Cast<Attribute>();
            var pathResult = _categoryListExtractionService.GetAttributePath(attributeList);

            if (pathResult.Valid) yield break;
            yield return new ValidationError
            {
                ErrorMessage = $"The Category path '{pathResult.Path}' doesn't point to an existing category. \n" +
                               $"The Category field is hidden untill the correct category is configured!",
                PropertyName = nameof(ICategorizable.Category),
                Severity = ValidationErrorSeverity.Warning,
                ValidationType = ValidationErrorType.AttributeMatched
            };
        }
    }
}

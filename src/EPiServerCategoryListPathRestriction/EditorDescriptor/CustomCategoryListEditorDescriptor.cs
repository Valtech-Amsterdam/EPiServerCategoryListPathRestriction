﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Cms.Shell.UI.ObjectEditing.EditorDescriptors;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.Attributes;
using Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.Services;

namespace Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.EditorDescriptor
{
    /// <summary>
    /// Custom editor descriptor for <see cref="CategoryList"/>s to check the <see cref="CategoryPathRestrictionAttribute"/> and change the category root for this type
    /// </summary>
    [EditorDescriptorRegistration(TargetType = typeof(CategoryList),
        EditorDescriptorBehavior = EditorDescriptorBehavior.OverrideDefault | EditorDescriptorBehavior.Default)]
    public class CustomCategoryListEditorDescriptor : CategoryListEditorDescriptor
    {
        private readonly ICategoryListExtractionService _categoryListExtractionService;

        /// <summary>
        /// Custom editor descriptor for <see cref="CategoryList"/>s to check the <see cref="CategoryPathRestrictionAttribute"/> and change the category root for this type
        /// </summary>
        /// <param name="categoryListExtractionService"></param>
        public CustomCategoryListEditorDescriptor(ICategoryListExtractionService categoryListExtractionService)
        {
            _categoryListExtractionService = categoryListExtractionService;
        }

        /// <inheritdoc />
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            var attributeList = attributes.ToList();
            base.ModifyMetadata(metadata, attributeList);

            var pathResult = _categoryListExtractionService.GetAttributePath(attributeList);
            if (!pathResult.Valid) HideCategoryField(metadata);

            metadata.EditorConfiguration["root"] = pathResult.SelectedCategoryRootID;
        }

        private static void HideCategoryField(ModelMetadata metadata)
        {
            // This will also raise a validation warning telling the user the category path is not found
            metadata.ShowForEdit = false;
            metadata.ShowForDisplay = false;
        }
    }
}
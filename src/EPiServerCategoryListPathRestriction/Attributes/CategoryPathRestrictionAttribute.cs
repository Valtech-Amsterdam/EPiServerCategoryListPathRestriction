﻿using System;
using Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.EditorDescriptor;

namespace Valtech.Amsterdam.Labs.EPiServerCategoryListPathRestriction.Attributes
{
    /// <summary>
    /// Restrict this category's options to a path string
    /// This is used by the <see cref="CustomCategoryListEditorDescriptor"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class CategoryPathRestrictionAttribute : Attribute
    {
        /// <summary>
        /// Restrict this category's options to a path string
        /// </summary>
        /// <param name="path">The path to restrict the category root to</param>
        public CategoryPathRestrictionAttribute(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException(nameof(path));
            Path = path;
        }

        /// <summary>
        /// The path to restrict the category root to
        /// </summary>
        public string Path { get; set; }
    }
}
